public with sharing class BasicEndpoints {
    @AuraEnabled
    public static String[] getOptions() {
        return new String[]{
            'A',
            'C',
            'F',
            'G',
            'O'
        };
    }
}
