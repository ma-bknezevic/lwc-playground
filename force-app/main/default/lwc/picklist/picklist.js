/* eslint-disable no-console */
import { LightningElement } from 'lwc';
import getOptions from '@salesforce/apex/BasicEndpoints.getOptions';

export default class Picklist extends LightningElement {
    options = [{ label: '1', value: 1 }];
    value = '';

    async connectedCallback() {
        const options = await getOptions();

        this.options = options.map(option => ({
            label: option,
            value: option
        }));
    }

    onChange(e) {
        console.log(e, 'changed');
    }
}